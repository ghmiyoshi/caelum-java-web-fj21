package br.com.caelum.jdbc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.caelum.jdbc.ConnectionFactory;
import br.com.caelum.jdbc.exception.DAOException;
import br.com.caelum.jdbc.modelo.Contato;

public class ContatoDao {

	// conexão com o banco de dados
	private Connection connection;

	public ContatoDao() {
		this.connection = new ConnectionFactory().getConnection();
	}

	public void adiciona(Contato contato) {
		String sql = "INSERT INTO contatos(nome, email, endereco, dataNascimento) VALUES(?,?,?,?)";

		try {
			// prepared statement para inserção
			PreparedStatement declaracao = connection.prepareStatement(sql);

			// seta os valores
			declaracao.setString(1, contato.getNome());
			declaracao.setString(2, contato.getEmail());
			declaracao.setString(2, contato.getEmail());
			declaracao.setString(3, contato.getEndereco());
			declaracao.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));

			// executa
			declaracao.execute();
			declaracao.close();
			connection.close();
		} catch (SQLException erro) {
			throw new DAOException("Erro no método adiciona");
		}
	}

	public List<Contato> lista() {
		String sql = "SELECT c.* FROM contatos c";

		try {
			// pega a conexão e o Statement
			PreparedStatement declaracao = connection.prepareStatement(sql);
			ResultSet rs = declaracao.executeQuery(); // retorna todos os registros de uma query

			List<Contato> contatos = new ArrayList<>();

			// itera no ResultSet
			while (rs.next()) {
				// criando o objeto contato
				Contato contato = new Contato();
				contato.setNome(rs.getString("nome")); // o método get do ResultSet retorna o valor de uma coluna no banco de dados
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));

				// montando a data através do Calendar
				Calendar data = Calendar.getInstance();
				data.setTime(rs.getDate("dataNascimento"));
				contato.setDataNascimento(data);

				// adicionando o objeto à lista
				contatos.add(contato);
			}
			rs.close();
			declaracao.close();
			connection.close();

			return contatos;
		} catch (SQLException erro) {
			throw new DAOException("Erro no método lista");
		}
	}

	public Contato pesquisa(long id) {
		try {
			PreparedStatement declaracao = connection.prepareStatement("SELECT c.* FROM contatos c WHERE c.id = ?");
			declaracao.setLong(1, id);

			ResultSet rs = declaracao.executeQuery();

			Contato contato = new Contato();

			if (rs.next()) {
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));

				Date date = rs.getDate("dataNascimento");

				if (date != null) {
					Calendar dataNascimento = Calendar.getInstance();
					dataNascimento.setTime(date);
					contato.setDataNascimento(dataNascimento);

				} else {
					return null;
				}
			}
			declaracao.close();
			connection.close();

			return contato;
		} catch (SQLException erro) {
			throw new DAOException("Erro no método pesquisa");
		}
	}

	public void altera(Contato contato) {
		try {
			PreparedStatement declaracao = connection
					.prepareStatement("UPDATE contatos SET nome=?, email=?, endereco=?, dataNascimento=? WHERE id=?");

			declaracao.setString(1, contato.getNome());
			declaracao.setString(2, contato.getEmail());
			declaracao.setString(3, contato.getEndereco());
			declaracao.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));
			declaracao.setLong(5, contato.getId());

			declaracao.execute();
			declaracao.close();
			connection.close();
		} catch (SQLException erro) {
			throw new DAOException("Erro no método altera");
		}
	}

	public void remove(Contato contato) {
		try {
			PreparedStatement declaracao = connection.prepareStatement("DELETE FROM contatos WHERE id = ?");

			declaracao.setLong(1, contato.getId());

			declaracao.execute();
			declaracao.close();
			connection.close();
		} catch (SQLException e) {
			throw new DAOException("Erro no método remove");
		}
	}

}
