package br.com.caelum.jdbc.teste;

import java.util.List;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class ListTest {

	/* 2.13 EXERCÍCIOS: LISTAGEM */

	public static void main(String[] args) {
		ContatoDao contatoDao = new ContatoDao();

		List<Contato> contatos = contatoDao.lista();

		for (Contato contato : contatos) {
			System.out.println(contato);
		}
	}

}
