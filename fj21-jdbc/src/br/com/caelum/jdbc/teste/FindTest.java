package br.com.caelum.jdbc.teste;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class FindTest {

	/* 2.15 EXERCÍCIOS OPCIONAIS */

	public static void main(String[] args) {
		ContatoDao contatoDao = new ContatoDao();

		Contato contato = contatoDao.pesquisa(1);

		System.out.println(contato);
	}

}
