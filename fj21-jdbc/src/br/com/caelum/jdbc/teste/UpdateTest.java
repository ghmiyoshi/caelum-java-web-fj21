package br.com.caelum.jdbc.teste;

import java.util.Calendar;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class UpdateTest {

	/* 2.16 OUTROS MÉTODOS PARA O SEU DAO */

	public static void main(String[] args) {
		Contato contato = new Contato();
		ContatoDao contatoDao = new ContatoDao();

		contato.setId((long) 1);
		contato.setNome("Hideki");
		contato.setEmail("hideki@hotmail.com");
		contato.setEndereco("Avenida Água Branca");

		Calendar calendar = Calendar.getInstance();
		calendar.set(1995, 8, 20);

		contato.setDataNascimento(calendar);

		contatoDao.altera(contato);

		System.out.println("Contato alterado com sucesso");
		System.out.println(contato);
	}

}
