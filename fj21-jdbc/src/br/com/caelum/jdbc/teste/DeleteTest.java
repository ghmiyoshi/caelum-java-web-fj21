package br.com.caelum.jdbc.teste;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class DeleteTest {

	/* 2.16 OUTROS MÉTODOS PARA O SEU DAO */

	public static void main(String[] args) {
		Contato contato = new Contato();
		ContatoDao contatoDao = new ContatoDao();

		contato.setId((long) 2);

		contatoDao.remove(contato);

		System.out.println("Contato removido com sucesso!");
	}

}
