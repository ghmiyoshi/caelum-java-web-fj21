package br.com.caelum.jdbc.teste;

import java.sql.Connection;
import java.sql.SQLException;

import br.com.caelum.jdbc.ConnectionFactory;

public class ConnectionTest {

	/* 2.6 EXERCÍCIOS: CONNECTIONFACTORY */

	public static void main(String[] args) throws SQLException {
		Connection conexao = new ConnectionFactory().getConnection();

		System.out.println("Conexão aberta!");
		conexao.close();
	}

}
