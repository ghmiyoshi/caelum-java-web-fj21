package br.com.caelum.jdbc.teste;

import java.util.Calendar;

import br.com.caelum.jdbc.dao.ContatoDao;
import br.com.caelum.jdbc.modelo.Contato;

public class InsertTest {

	/* 2.11 EXERCÍCIOS: JAVABEANS E CONTATODAO */

	public static void main(String[] args) {
		// pronto para gravar
		Contato contato = new Contato();

		contato.setNome("Gabriel");
		contato.setEmail("gabriel.hideki@hotmail.com");
		contato.setEndereco("R. Vergueiro 3185");
		contato.setDataNascimento(Calendar.getInstance());

		// gravar nessa conexão
		ContatoDao contatoDao = new ContatoDao();

		// método elegante
		contatoDao.adiciona(contato);

		System.out.println("Gravado!");

	}

}
