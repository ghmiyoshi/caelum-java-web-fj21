package br.com.caelum.mvc.logica;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.agenda.dao.ContatoDao;
import br.com.caelum.agenda.model.Contato;

public class ListaContatosLogic implements Logica {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// busco a conexão pendurada na requisição
		Connection conexao = (Connection) request.getAttribute("conexao");
		
		// passo a conexão no construtor
		ContatoDao contatoDao = new ContatoDao(conexao);

		// Monta a lista de contatos
		List<Contato> contatos = contatoDao.getLista();

		// Guarda a lista no request
		request.setAttribute("contatos", contatos);
		
		System.out.println("Listando contatos...");

		return "forward:/WEB-INF/jsp/lista-contatos.jsp";
	}

}
