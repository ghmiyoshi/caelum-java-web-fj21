package br.com.caelum.mvc.logica;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.agenda.dao.ContatoDao;
import br.com.caelum.agenda.model.Contato;

public class RemoveContatoLogic implements Logica {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Long id = Long.parseLong(request.getParameter("id"));
		
		Connection conexao = (Connection) request.getAttribute("conexao");
		
		ContatoDao contatoDao = new ContatoDao(conexao);
		
		Contato contato = new Contato();
		contato.setId(id);
		
		contatoDao.exclui(contato);
		
		System.out.println("Excluindo contato...");
		
		return "redirect:mvc?logica=ListaContatosLogic";
	}

}
