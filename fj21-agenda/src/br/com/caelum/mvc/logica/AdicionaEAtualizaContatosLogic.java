package br.com.caelum.mvc.logica;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.agenda.dao.ContatoDao;
import br.com.caelum.agenda.model.Contato;

public class AdicionaEAtualizaContatosLogic implements Logica {

	@Override
	public String executa(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Connection conexao = (Connection) request.getAttribute("conexao"); // recupero o objeto que foi "pendurado" no request do filtro
		ContatoDao contatoDao = new ContatoDao(conexao);
		Contato contato = new Contato();

		contato.setNome(request.getParameter("nome"));
		contato.setEmail(request.getParameter("email"));
		contato.setEndereco(request.getParameter("endereco"));
		
		// fazendo a conversão da data
		String dataEmTexto = request.getParameter("dataNascimento");
		Calendar dataNascimento = null;
		
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataEmTexto);
			dataNascimento = Calendar.getInstance();
			dataNascimento.setTime(date);
		} catch (ParseException erro) {
			System.out.println("Erro de conversão da data " + erro);
		}
		
		contato.setDataNascimento(dataNascimento);
		
		if (request.getParameter("id") == null) {
			System.out.println("Adicionando contato com a nova lógica...");

			contatoDao.adiciona(contato);
		} else {
			System.out.println("Alterando contato com a nova lógica...");

			contato.setId(Long.parseLong(request.getParameter("id")));

			contatoDao.altera(contato);
		}

		return "redirect:mvc?logica=ListaContatosLogic";
	}

}
