package br.com.caelum.mvc.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.caelum.mvc.logica.Logica;

@WebServlet("/mvc")
public class ControllerServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String parametro = request.getParameter("logica");
		String nomeDaClasse = "br.com.caelum.mvc.logica." + parametro;
		
		try {
			Class<?> classe = Class.forName(nomeDaClasse);
			Logica logica = (Logica) classe.newInstance();
			String pagina = logica.executa(request, response);
			
			String[] tipoEEndereco = pagina.split(":");

			if (tipoEEndereco[0].equals("forward")) {
				RequestDispatcher rd = request.getRequestDispatcher(tipoEEndereco[1]); // "WEB-INF/views/" + 
				rd.forward(request, response);
			} else {
				response.sendRedirect(tipoEEndereco[1]);
			}
			
		} catch (Exception erro) {
			throw new ServletException("A lógica de negócios causou uma exceção", erro);
		}
	}

}
