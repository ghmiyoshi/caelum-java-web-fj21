package br.com.caelum.agenda.filtro;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import br.com.caelum.agenda.connection.ConnectionFactory;

@WebFilter("/*")
public class FiltroConexao implements Filter {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		try {
			// abro uma conexão
			Connection connection = new ConnectionFactory().getConnection();
			
			// "penduro um objeto no Request"
			request.setAttribute("conexao", connection);
			
			// indico que o processamento do request deve prosseguir
			chain.doFilter(request, response);
			
			// fecho a conexão
			connection.close();
		} catch (SQLException erro) {
			erro.printStackTrace();
		}
	}

}
