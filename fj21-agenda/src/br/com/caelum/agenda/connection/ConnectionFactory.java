package br.com.caelum.agenda.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	public Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://localhost/fj21", "root", "");
		} catch (SQLException | ClassNotFoundException erro) {
			throw new RuntimeException(erro); // Relançando como RuntimeException para que o código que chamar a fábrica de conexões não fique acoplado com a API de JDBC
		}
	}

}
