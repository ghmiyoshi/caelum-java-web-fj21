package br.com.caelum.agenda.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.caelum.agenda.connection.ConnectionFactory;
import br.com.caelum.agenda.exception.DAOException;
import br.com.caelum.agenda.model.Contato;

public class ContatoDao {

	// conexão com o banco de dados
	private Connection connection;

	public ContatoDao(Connection conexao) {
		this.connection = conexao;
	}
	
	public ContatoDao() {

	}
	
	public void adiciona(Contato contato) {
		try {
			// prepared statement para inserção
			PreparedStatement declaracao = connection
					.prepareStatement("INSERT INTO contatos(nome, email, endereco, dataNascimento) VALUES(?, ?, ?, ?)");

			// seta os valores
			declaracao.setString(1, contato.getNome());
			declaracao.setString(2, contato.getEmail());
			declaracao.setString(3, contato.getEndereco());
			declaracao.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));

			// executa
			declaracao.execute();
			declaracao.close();
			connection.close();
		} catch (SQLException erro) {
			throw new DAOException("Erro no método adiciona " + erro);
		}
	}

	public List<Contato> getLista() {
		try {
			// pega a conexão e o Statement
			PreparedStatement declaracao = connection.prepareStatement("SELECT c.* FROM contatos c");
			ResultSet rs = declaracao.executeQuery(); // retorna todos os registros de uma query

			List<Contato> contatos = new ArrayList<>();

			// itera no ResultSet
			while (rs.next()) {
				// cria o objeto contato
				Contato contato = new Contato();
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));

				// monta a data através do Calendar
				Calendar data = Calendar.getInstance();
				data.setTime(rs.getDate("dataNascimento"));
				contato.setDataNascimento(data);

				// adiciona o objeto na lista
				contatos.add(contato);
			}
			rs.close();
			declaracao.close();
			connection.close();

			return contatos;
		} catch (SQLException erro) {
			throw new DAOException("Erro no método lista " + erro);
		}
	}
	
	public void exclui(Contato contato) {
		try {
			PreparedStatement declaracao = connection.prepareStatement("DELETE c FROM contatos c WHERE c.id=?");
			declaracao.setLong(1, contato.getId());

			declaracao.execute();
			declaracao.close();
			connection.close();
		} catch (SQLException erro) {
			throw new DAOException("Erro no método exclui " + erro);
		}
	}
	
	public Contato pesquisa(Long id) {
		try {
			PreparedStatement declaracao = connection.prepareStatement("SELECT c.* FROM contatos c WHERE c.id=?");
			declaracao.setLong(1, id);
			
			ResultSet rs = declaracao.executeQuery();
			
			Contato contato = new Contato();
			
			if (rs.next()) {
				contato.setId(rs.getLong("id"));
				contato.setNome(rs.getString("nome"));
				contato.setEmail(rs.getString("email"));
				contato.setEndereco(rs.getString("endereco"));

				Date date = rs.getDate("dataNascimento");

				if (date != null) {
					Calendar dataNascimento = Calendar.getInstance();
					dataNascimento.setTime(date);
					contato.setDataNascimento(dataNascimento);

				} else {
					return null;
				}
			}
			declaracao.close();
			connection.close();

			return contato;
		} catch (SQLException erro) {
			throw new DAOException("Erro no método pesquisa " + erro);
		}
	}

	public void altera(Contato contato) {
		try {
			PreparedStatement declaracao = connection
					.prepareStatement("UPDATE contatos SET nome=?, email=?, endereco=?, dataNascimento=? WHERE id=?");

			declaracao.setString(1, contato.getNome());
			declaracao.setString(2, contato.getEmail());
			declaracao.setString(3, contato.getEndereco());
			declaracao.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));
			declaracao.setLong(5, contato.getId());

			declaracao.execute();
			declaracao.close();
			connection.close();
		} catch (SQLException erro) {
			throw new DAOException("Erro no método altera " + erro);
		}
	}

}
