<%@ attribute name="id" required="true" %>

<!-- o Javascript do calendário e o input foram gerados através de nossa Taglib -->
<input type="text" id="${id}" name="${id}">
<script>
	$("#${id}").datepicker({dateFormat: 'dd/mm/yy'});
</script>
