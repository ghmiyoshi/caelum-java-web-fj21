<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>FJ21 - Agenda</title>
</head>
<body>
	<c:import url="cabecalho.jsp"></c:import>
	
	<h1>Agenda de Contatos da Caelum</h1>
	<hr/>

	<!-- cria o DAO -->
	<!--<jsp:useBean id="dao" class="br.com.caelum.agenda.dao.ContatoDao" /> -->

	<table>
		<!-- percorre contatos montando as linhas da tabela -->
		<tr>
			<td>Nome</td>
			<td>E-mail</td>
			<td>Endere�o</td>
			<td>Data de nascimento</td>
		</tr>
		<tr>
			<c:forEach var="contato" items="${contatos}" varStatus="id">
				<tr bgcolor="#${id.count % 2 == 0 ? 'aaee88' : 'ffffff' }">
					<td>${contato.nome}</td>
					<td>
					<!-- 
					<c:if test="${not empty contato.email }">
						<a href="mailto:${contato.email }">${contato.email }</a>						
					</c:if>  -->
					
					<c:choose>
						<c:when test="${not empty contato.email}">
						<a href="mailto:${contato.email}">${contato.email}</a>
						</c:when>
						<c:otherwise>
						E-mail n�o informado
						</c:otherwise>
					</c:choose>
					</td>
					<td>${contato.endereco}</td>
					<td><fmt:formatDate value="${contato.dataNascimento.time}" pattern="dd/MM/yyyy"/></td>
					<td><a href="mvc?logica=EditaContatoLogic&id=${contato.id }">Alterar</a></td>
					<td><a href="mvc?logica=RemoveContatoLogic&id=${contato.id }">Remover</a></td>
				</tr>
			</c:forEach>
		</tr>
	</table>
	<hr/>
	
	<c:import url="rodape.jsp"></c:import>
</body>
</html>