<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="caelum" %>
<!DOCTYPE html>
<html>
<head>
	<link href="css/jquery.css" rel="stylesheet">
	<script src="js/jquery.js"></script>
	<script src="js/jquery-ui.js"></script>
<meta charset="UTF-8">
<title>FJ21 - Agenda</title>
</head>
<body>

	<c:import url="cabecalho.jsp"></c:import>
	
	<h1>Agenda de Contatos da Caelum</h1>
	<hr/>

	<form action="mvc?logica=AdicionaEAtualizaContatosLogic" method="post">
		<label>Nome:<input type="text" name="nome"></label><br/>
		<label>E-mail:<input type="email" name="email"></label><br/>
		<label>Endereço:<input type="text" name="endereco"></label><br/>
		<label>Data de nascimento:<caelum:campoData id="dataNascimento"/></label><br/>
		<button>Cadastrar</button>
	</form>
	
	<hr/>
	<c:import url="rodape.jsp"></c:import>
</body>
</html>