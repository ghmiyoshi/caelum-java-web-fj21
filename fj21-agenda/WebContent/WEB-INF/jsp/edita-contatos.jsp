<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="caelum" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:import url="cabecalho.jsp"></c:import>
	
	<h1>Agenda de Contatos da Caelum</h1>
	<hr/>

	<form action="mvc?logica=AdicionaEAtualizaContatosLogic" method="post">
		<input type="hidden" name="id" value="${contato.id }">
		<label>Nome:<input type="text" name="nome" value="${contato.nome }"></label><br/>
		<label>E-mail:<input type="email" name="email" value="${contato.email }"></label><br/>
		<label>Endereço:<input type="text" name="endereco" value="${contato.endereco }"></label><br/>
		<label>Data de nascimento:<input name="dataNascimento" value=<fmt:formatDate value="${contato.dataNascimento.time }" pattern="dd/MM/yyyy"/>></label><br/>		
		<button>Alterar</button>
	</form>
	
	<hr/>
	<c:import url="rodape.jsp"></c:import>
</body>
</html>