<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="java.util.*,
		br.com.caelum.agenda.dao.*,
		br.com.caelum.agenda.model.*,
		java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>FJ21 - Agenda</title>
</head>
<body>
	<table>
		<tr>
			<td>Nome</td>
			<td>Email</td>
			<td>Endereço</td>
			<td>Data de Nascimento</td>
		</tr>
		<%
			ContatoDao dao = new ContatoDao();
			List<Contato> contatos = dao.getLista();
			for (Contato contato : contatos) {
		%>
		<tr>
			<td><%=contato.getNome()%></td>
			<td><%=contato.getEmail()%></td>
			<td><%=contato.getEndereco()%></td>
			
			<!-- Formatando a data para dd/MM/yyyy -->
			<% SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy"); %> 
			<% String dataFormatada = data.format(contato.getDataNascimento().getTime()); %>
			<td><%=dataFormatada%></td>
		</tr>
		<%
			}
		%>

	
	</table>
</body>
</html>