package br.com.caelum.tarefas.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.caelum.tarefas.dao.JdbcTarefaDao;
import br.com.caelum.tarefas.modelo.Tarefa;

@Controller
public class TarefasController {
	
	private static final Logger LOG = Logger.getLogger(TarefasController.class);

	@RequestMapping("/novaTarefa")
	public String formTarefa() {
		LOG.info("Acessando método formTarefa");
		return "tarefa/formulario";
	}

	@RequestMapping("/adicionaTarefa")
	/* @Valid aviso o Spring para executar a validação do objeto antes de chamar o método
	   O Spring guarda os erros de validação em um objeto do tipo BindingResult
	 */
	public String adiciona(@Valid Tarefa tarefa, BindingResult result) {
		LOG.info("Acessando método adiciona");
		
		if (result.hasFieldErrors("descricao")) { // verifico se existe um erro de validação relacionado com o atributo descricao da tarefa
			return "tarefa/formulario";
		}

		JdbcTarefaDao jdbcTarefaDao = new JdbcTarefaDao();
		jdbcTarefaDao.adiciona(tarefa);

		return "tarefa/sucesso";
	}
	
	@RequestMapping("/listaTarefas")
	// Uso Model para passar um objeto para a view, no caso a lista de tarefas
	public String lista(Model model) {
		LOG.info("Acessando método lista");
		
		JdbcTarefaDao jdbcTarefaDao = new JdbcTarefaDao();
		List<Tarefa> tarefas = jdbcTarefaDao.lista();
		
		model.addAttribute("tarefas",tarefas);
		
		return "tarefa/lista";
	}
	
	@RequestMapping("/removeTarefa")
	public String remove(Tarefa tarefa) {
		LOG.info("Acessando método remove");
		
		JdbcTarefaDao jdbcTarefaDao = new JdbcTarefaDao();
		jdbcTarefaDao.remove(tarefa);
		
		return "redirect:listaTarefas"; // Posso fazer um redirecionamento no lado do servidor (forward) ou pelo navegador no lado do cliente (redirect)
	}
	
	@RequestMapping("/mostraTarefa")
	public String mostra(Long id, Model model) {
		LOG.info("Acessando método mostra");
		
		JdbcTarefaDao jdbcTarefaDao = new JdbcTarefaDao();
		
		model.addAttribute("tarefa", jdbcTarefaDao.buscaPorId(id));
		
		return "tarefa/mostra";
	}
	
	@RequestMapping("/alteraTarefa")
	public String altera(Tarefa tarefa) {
		LOG.info("Acessando método altera");
		
		JdbcTarefaDao jdbcTarefaDao = new JdbcTarefaDao();
		jdbcTarefaDao.altera(tarefa);
		
		return "redirect:listaTarefas";
	}
	
}
